﻿using AutoMapper;
using WorkLoaderOfProfessor.DAL.Models;
using WorkLoaderOfProfessor.Service.DTO.ProfessorsDto;

namespace WorkLoaderOfProfessor.Mapper
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile() 
        {
            CreateMap<ProfessorsDto, ProfessorModel>();
            CreateMap<ProfessorModel, ProfessorsDto>();
        }
    }
}
