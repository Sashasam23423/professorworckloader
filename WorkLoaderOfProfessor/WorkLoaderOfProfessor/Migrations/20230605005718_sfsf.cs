﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace WorkLoaderOfProfessor.Migrations
{
    /// <inheritdoc />
    public partial class sfsf : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {

            migrationBuilder.AlterColumn<int>(
                name: "CountStudent",
                table: "DistributionForSubjects",
                type: "integer",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "integer",
                oldNullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "CountStudent",
                table: "DistributionForSubjects",
                type: "integer",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<string>(
                name: "SubjectType",
                table: "DistributionForSubjects",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.InsertData(
                table: "Flows",
                columns: new[] { "Id", "Half", "NumberStream" },
                values: new object[,]
                {
                    { 1, 1, 2 },
                    { 2, 1, 3 },
                    { 3, 2, 1 },
                    { 4, 1, 1 },
                    { 5, 3, 3 }
                });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "CountStudent", "DirectionOfStudy", "Half", "Name" },
                values: new object[,]
                {
                    { 1, 40, "ФИИТ", 1, "12" },
                    { 2, 42, "ФИИТ", 1, "11" },
                    { 3, 45, "Моис", 1, "21" },
                    { 4, 47, "Моис", 1, "22" },
                    { 5, 42, "ПМИ", 1, "32" },
                    { 6, 41, "Моис", 1, "23" },
                    { 7, 43, "ПМИ", 1, "31" },
                    { 8, 46, "ПИ", 1, "7" },
                    { 9, 38, "ПИ", 1, "8" },
                    { 10, 42, "ПИ", 1, "9" },
                    { 11, 43, "ФИИТ", 2, "11" },
                    { 12, 39, "ФИИТ", 2, "12" }
                });

            migrationBuilder.InsertData(
                table: "Professors",
                columns: new[] { "Id", "CountOfStudent", "Hours", "LastName", "Like", "MiddleName", "Name", "NumberOfHalfGroup", "NumbersOfFlows", "NumbersOfGroups", "Post" },
                values: new object[,]
                {
                    { 1, 0, 0f, "Brad", 0f, "", "Kirill", 0, 0, 0, "" },
                    { 2, 0, 0f, "Vin", 0f, "", "Alex", 0, 0, 0, "" },
                    { 3, 0, 0f, "Skarup", 0f, "", "Rostislav", 0, 0, 0, "" }
                });

            migrationBuilder.InsertData(
                table: "Subjects",
                columns: new[] { "Id", "Name", "Type" },
                values: new object[,]
                {
                    { 1, "Мат. анализ", "Лекция" },
                    { 2, "Мат. анализ", "Практика" },
                    { 3, "ЯМП", "Лекция" },
                    { 4, "ЯМП", "Лабораторные работы" },
                    { 5, "ОС", "Лекция" },
                    { 6, "ОС", "Лабораторные работы" },
                    { 7, "ЯМП", "Практика" }
                });

            migrationBuilder.InsertData(
                table: "DistributionForSubjects",
                columns: new[] { "Id", "CountStudent", "FlowId", "GroupId", "Hours", "ProfessorId", "Semester", "SubjectId", "SubjectType" },
                values: new object[,]
                {
                    { 1, null, null, 1, 96f, null, 0, 2, "Практика" },
                    { 2, null, null, 2, 96f, null, 0, 2, "Практика" },
                    { 3, null, null, 4, 96f, null, 0, 2, "Практика" },
                    { 4, null, null, 3, 96f, null, 0, 2, "Практика" },
                    { 5, null, null, 5, 96f, null, 0, 2, "Практика" },
                    { 6, null, null, 6, 96f, null, 0, 2, "Практика" },
                    { 7, null, null, 7, 96f, null, 0, 2, "Практика" },
                    { 8, null, null, 9, 24f, null, 0, 7, "Практика" },
                    { 9, null, null, 8, 24f, null, 0, 7, "Практика" },
                    { 10, null, null, 7, 24f, null, 0, 7, "Практика" },
                    { 11, null, null, 6, 24f, null, 0, 7, "Практика" },
                    { 12, null, null, 5, 24f, null, 0, 7, "Практика" },
                    { 13, null, null, 4, 24f, null, 0, 7, "Практика" },
                    { 14, null, null, 3, 24f, null, 0, 7, "Практика" },
                    { 15, null, null, 2, 24f, null, 0, 7, "Практика" },
                    { 16, null, null, 1, 24f, null, 0, 7, "Практика" },
                    { 17, null, null, 11, 24f, null, 0, 7, "Практика" },
                    { 18, null, null, 10, 24f, null, 0, 7, "Практика" },
                    { 19, null, null, 12, 24f, null, 0, 7, "Практика" },
                    { 20, null, null, 8, 96f, null, 0, 2, "Практика" },
                    { 21, null, null, 9, 96f, null, 0, 2, "Практика" },
                    { 22, null, null, 10, 96f, null, 0, 2, "Практика" },
                    { 23, null, null, 11, 96f, null, 0, 2, "Практика" },
                    { 24, null, null, 12, 96f, null, 0, 2, "Практика" },
                    { 25, null, 1, null, 96f, null, 0, 1, "Лекция" },
                    { 26, null, 1, null, 48f, null, 0, 3, "Лекция" },
                    { 27, null, 1, null, 24f, null, 0, 5, "Лекция" },
                    { 28, null, 3, null, 24f, null, 0, 5, "Лекция" },
                    { 29, null, 2, null, 48f, null, 0, 3, "Лекция" },
                    { 30, null, 2, null, 24f, null, 0, 5, "Лекция" },
                    { 31, null, 3, null, 96f, null, 0, 1, "Лекция" },
                    { 32, null, 2, null, 96f, null, 0, 1, "Лекция" },
                    { 33, null, null, 1, 19f, null, 0, 4, "Лабораторные работы" },
                    { 34, null, null, 1, 19f, null, 0, 4, "Лабораторные работы" },
                    { 35, null, null, 2, 19f, null, 0, 4, "Лабораторные работы" },
                    { 36, null, null, 2, 19f, null, 0, 4, "Лабораторные работы" },
                    { 37, null, null, 3, 19f, null, 0, 4, "Лабораторные работы" },
                    { 38, null, null, 3, 19f, null, 0, 4, "Лабораторные работы" },
                    { 39, null, null, 1, 12f, null, 0, 6, "Лабораторные работы" },
                    { 40, null, null, 1, 12f, null, 0, 6, "Лабораторные работы" },
                    { 41, null, null, 4, 19f, null, 0, 4, "Лабораторные работы" },
                    { 42, null, null, 4, 19f, null, 0, 4, "Лабораторные работы" },
                    { 43, null, null, 4, 12f, null, 0, 6, "Лабораторные работы" },
                    { 44, null, null, 4, 12f, null, 0, 6, "Лабораторные работы" },
                    { 45, null, null, 5, 19f, null, 0, 4, "Лабораторные работы" },
                    { 46, null, null, 5, 19f, null, 0, 4, "Лабораторные работы" },
                    { 47, null, null, 5, 12f, null, 0, 6, "Лабораторные работы" },
                    { 48, null, null, 5, 12f, null, 0, 6, "Лабораторные работы" },
                    { 49, null, null, 6, 19f, null, 0, 4, "Лабораторные работы" },
                    { 50, null, null, 6, 19f, null, 0, 4, "Лабораторные работы" },
                    { 51, null, null, 6, 12f, null, 0, 6, "Лабораторные работы" },
                    { 52, null, null, 6, 12f, null, 0, 6, "Лабораторные работы" },
                    { 53, null, null, 7, 19f, null, 0, 4, "Лабораторные работы" },
                    { 54, null, null, 7, 19f, null, 0, 4, "Лабораторные работы" },
                    { 55, null, null, 7, 12f, null, 0, 6, "Лабораторные работы" },
                    { 56, null, null, 7, 12f, null, 0, 6, "Лабораторные работы" },
                    { 57, null, null, 8, 19f, null, 0, 4, "Лабораторные работы" },
                    { 58, null, null, 8, 19f, null, 0, 4, "Лабораторные работы" },
                    { 59, null, null, 8, 12f, null, 0, 6, "Лабораторные работы" },
                    { 60, null, null, 8, 12f, null, 0, 6, "Лабораторные работы" },
                    { 61, null, null, 9, 19f, null, 0, 4, "Лабораторные работы" },
                    { 62, null, null, 9, 19f, null, 0, 4, "Лабораторные работы" },
                    { 63, null, null, 9, 12f, null, 0, 6, "Лабораторные работы" },
                    { 64, null, null, 9, 12f, null, 0, 6, "Лабораторные работы" },
                    { 65, null, null, 10, 19f, null, 0, 4, "Лабораторные работы" },
                    { 66, null, null, 10, 19f, null, 0, 4, "Лабораторные работы" },
                    { 67, null, null, 10, 19f, null, 0, 4, "Лабораторные работы" },
                    { 68, null, null, 10, 19f, null, 0, 4, "Лабораторные работы" },
                    { 69, null, null, 11, 19f, null, 0, 4, "Лабораторные работы" },
                    { 70, null, null, 11, 19f, null, 0, 4, "Лабораторные работы" },
                    { 71, null, null, 11, 12f, null, 0, 6, "Лабораторные работы" },
                    { 72, null, null, 11, 12f, null, 0, 6, "Лабораторные работы" },
                    { 73, null, null, 12, 19f, null, 0, 4, "Лабораторные работы" },
                    { 74, null, null, 12, 19f, null, 0, 4, "Лабораторные работы" },
                    { 75, null, null, 12, 12f, null, 0, 6, "Лабораторные работы" },
                    { 76, null, null, 12, 12f, null, 0, 6, "Лабораторные работы" }
                });
        }
    }
}
