﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WorkLoaderOfProfessor.Migrations
{
    /// <inheritdoc />
    public partial class asfasdf : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Post",
                table: "Professors",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AlterColumn<float>(
                name: "Hours",
                table: "DistributionForSubjects",
                type: "real",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<int>(
                name: "CountStudent",
                table: "DistributionForSubjects",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Semester",
                table: "DistributionForSubjects",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "SubjectType",
                table: "DistributionForSubjects",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 8,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 9,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 10,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 11,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 12,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 13,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 14,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 15,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 16,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 17,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 18,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 19,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 20,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 21,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 22,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 23,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 24,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Практика" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 25,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Лекция" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 26,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 48f, 0, "Лекция" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 27,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Лекция" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 28,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Лекция" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 29,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 48f, 0, "Лекция" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 30,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 24f, 0, "Лекция" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 31,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Лекция" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 32,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 96f, 0, "Лекция" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 33,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 34,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 35,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 36,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 37,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 38,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 39,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 40,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 41,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 42,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 43,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 44,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 45,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 46,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 47,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 48,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 49,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 50,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 51,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 52,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 53,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 54,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 55,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 56,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 57,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 58,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 59,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 60,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 61,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 62,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 63,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 64,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 65,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 66,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 67,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 68,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 69,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 70,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 71,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 72,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 73,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 74,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 19f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 75,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 76,
                columns: new[] { "CountStudent", "Hours", "Semester", "SubjectType" },
                values: new object[] { null, 12f, 0, "Лабораторные работы" });

            migrationBuilder.UpdateData(
                table: "Professors",
                keyColumn: "Id",
                keyValue: 1,
                column: "Post",
                value: "");

            migrationBuilder.UpdateData(
                table: "Professors",
                keyColumn: "Id",
                keyValue: 2,
                column: "Post",
                value: "");

            migrationBuilder.UpdateData(
                table: "Professors",
                keyColumn: "Id",
                keyValue: 3,
                column: "Post",
                value: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Post",
                table: "Professors");

            migrationBuilder.DropColumn(
                name: "CountStudent",
                table: "DistributionForSubjects");

            migrationBuilder.DropColumn(
                name: "Semester",
                table: "DistributionForSubjects");

            migrationBuilder.DropColumn(
                name: "SubjectType",
                table: "DistributionForSubjects");

            migrationBuilder.AlterColumn<int>(
                name: "Hours",
                table: "DistributionForSubjects",
                type: "integer",
                nullable: false,
                oldClrType: typeof(float),
                oldType: "real");

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 1,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 2,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 3,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 4,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 5,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 6,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 7,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 8,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 9,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 10,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 11,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 12,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 13,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 14,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 15,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 16,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 17,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 18,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 19,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 20,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 21,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 22,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 23,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 24,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 25,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 26,
                column: "Hours",
                value: 48);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 27,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 28,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 29,
                column: "Hours",
                value: 48);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 30,
                column: "Hours",
                value: 24);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 31,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 32,
                column: "Hours",
                value: 96);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 33,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 34,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 35,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 36,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 37,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 38,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 39,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 40,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 41,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 42,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 43,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 44,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 45,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 46,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 47,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 48,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 49,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 50,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 51,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 52,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 53,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 54,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 55,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 56,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 57,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 58,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 59,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 60,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 61,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 62,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 63,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 64,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 65,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 66,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 67,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 68,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 69,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 70,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 71,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 72,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 73,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 74,
                column: "Hours",
                value: 19);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 75,
                column: "Hours",
                value: 12);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 76,
                column: "Hours",
                value: 12);
        }
    }
}
