﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WorkLoaderOfProfessor.Migrations
{
    /// <inheritdoc />
    public partial class initialsadfsad : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DistributionForSubjects_Groups_FlowId",
                table: "DistributionForSubjects");

            migrationBuilder.AddForeignKey(
                name: "FK_DistributionForSubjects_Flows_FlowId",
                table: "DistributionForSubjects",
                column: "FlowId",
                principalTable: "Flows",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DistributionForSubjects_Flows_FlowId",
                table: "DistributionForSubjects");

            migrationBuilder.AddForeignKey(
                name: "FK_DistributionForSubjects_Groups_FlowId",
                table: "DistributionForSubjects",
                column: "FlowId",
                principalTable: "Groups",
                principalColumn: "Id");
        }
    }
}
