﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace WorkLoaderOfProfessor.Migrations
{
    /// <inheritdoc />
    public partial class initialsadfsadgdfsgsdfg : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 29,
                column: "FlowId",
                value: 2);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 30,
                column: "FlowId",
                value: 2);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 32,
                column: "FlowId",
                value: 2);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 29,
                column: "FlowId",
                value: 4);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 30,
                column: "FlowId",
                value: 4);

            migrationBuilder.UpdateData(
                table: "DistributionForSubjects",
                keyColumn: "Id",
                keyValue: 32,
                column: "FlowId",
                value: 4);
        }
    }
}
