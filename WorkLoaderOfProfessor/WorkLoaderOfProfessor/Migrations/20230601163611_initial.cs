﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace WorkLoaderOfProfessor.Migrations
{
    /// <inheritdoc />
    public partial class initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Flows",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NumberStream = table.Column<int>(type: "integer", nullable: false),
                    Half = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Flows", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false),
                    DirectionOfStudy = table.Column<string>(type: "text", nullable: false),
                    Half = table.Column<int>(type: "integer", nullable: false),
                    CountStudent = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Professors",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false),
                    MiddleName = table.Column<string>(type: "text", nullable: true),
                    LastName = table.Column<string>(type: "text", nullable: false),
                    Hours = table.Column<float>(type: "real", nullable: false),
                    NumbersOfFlows = table.Column<int>(type: "integer", nullable: false),
                    NumbersOfGroups = table.Column<int>(type: "integer", nullable: false),
                    NumberOfHalfGroup = table.Column<int>(type: "integer", nullable: false),
                    CountOfStudent = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Professors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Subjects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Name = table.Column<string>(type: "text", nullable: false),
                    Type = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Subjects", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "FlowModelGroupModel",
                columns: table => new
                {
                    FlowsId = table.Column<int>(type: "integer", nullable: false),
                    GroupsId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FlowModelGroupModel", x => new { x.FlowsId, x.GroupsId });
                    table.ForeignKey(
                        name: "FK_FlowModelGroupModel_Flows_FlowsId",
                        column: x => x.FlowsId,
                        principalTable: "Flows",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_FlowModelGroupModel_Groups_GroupsId",
                        column: x => x.GroupsId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DistributionForSubjects",
                columns: table => new
                {
                    Id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    SubjectId = table.Column<int>(type: "integer", nullable: false),
                    GroupId = table.Column<int>(type: "integer", nullable: true),
                    FlowId = table.Column<int>(type: "integer", nullable: true),
                    ProfessorId = table.Column<int>(type: "integer", nullable: true),
                    Hours = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DistributionForSubjects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DistributionForSubjects_Groups_FlowId",
                        column: x => x.FlowId,
                        principalTable: "Groups",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_DistributionForSubjects_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_DistributionForSubjects_Professors_ProfessorId",
                        column: x => x.ProfessorId,
                        principalTable: "Professors",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_DistributionForSubjects_Subjects_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subjects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Flows",
                columns: new[] { "Id", "Half", "NumberStream" },
                values: new object[,]
                {
                    { 1, 1, 2 },
                    { 2, 1, 3 },
                    { 3, 2, 1 },
                    { 4, 1, 1 },
                    { 5, 3, 3 }
                });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "CountStudent", "DirectionOfStudy", "Half", "Name" },
                values: new object[,]
                {
                    { 1, 40, "ФИИТ", 1, "12" },
                    { 2, 42, "ФИИТ", 1, "11" },
                    { 3, 45, "Моис", 1, "21" },
                    { 4, 47, "Моис", 1, "22" },
                    { 5, 42, "ПМИ", 1, "32" },
                    { 6, 41, "Моис", 1, "23" },
                    { 7, 43, "ПМИ", 1, "31" },
                    { 8, 46, "ПИ", 1, "7" },
                    { 9, 38, "ПИ", 1, "8" },
                    { 10, 42, "ПИ", 1, "9" },
                    { 11, 43, "ФИИТ", 2, "11" },
                    { 12, 39, "ФИИТ", 2, "12" }
                });

            migrationBuilder.InsertData(
                table: "Professors",
                columns: new[] { "Id", "CountOfStudent", "Hours", "LastName", "MiddleName", "Name", "NumberOfHalfGroup", "NumbersOfFlows", "NumbersOfGroups" },
                values: new object[,]
                {
                    { 1, 0, 0f, "Brad", "", "Kirill", 0, 0, 0 },
                    { 2, 0, 0f, "Vin", "", "Alex", 0, 0, 0 },
                    { 3, 0, 0f, "Skarup", "", "Rostislav", 0, 0, 0 }
                });

            migrationBuilder.InsertData(
                table: "Subjects",
                columns: new[] { "Id", "Name", "Type" },
                values: new object[,]
                {
                    { 1, "Мат. анализ", "Лекция" },
                    { 2, "Мат. анализ", "Практика" },
                    { 3, "ЯМП", "Лекция" },
                    { 4, "ЯМП", "Лабораторные работы" },
                    { 5, "ОС", "Лекция" },
                    { 6, "ОС", "Лабораторные работы" },
                    { 7, "ЯМП", "Практика" }
                });

            migrationBuilder.InsertData(
                table: "DistributionForSubjects",
                columns: new[] { "Id", "FlowId", "GroupId", "Hours", "ProfessorId", "SubjectId" },
                values: new object[,]
                {
                    { 1, null, 1, 96, null, 2 },
                    { 2, null, 2, 96, null, 2 },
                    { 3, null, 4, 96, null, 2 },
                    { 4, null, 3, 96, null, 2 },
                    { 5, null, 5, 96, null, 2 },
                    { 6, null, 6, 96, null, 2 },
                    { 7, null, 7, 96, null, 2 },
                    { 8, null, 9, 24, null, 7 },
                    { 9, null, 8, 24, null, 7 },
                    { 10, null, 7, 24, null, 7 },
                    { 11, null, 6, 24, null, 7 },
                    { 12, null, 5, 24, null, 7 },
                    { 13, null, 4, 24, null, 7 },
                    { 14, null, 3, 24, null, 7 },
                    { 15, null, 2, 24, null, 7 },
                    { 16, null, 1, 24, null, 7 },
                    { 17, null, 11, 24, null, 7 },
                    { 18, null, 10, 24, null, 7 },
                    { 19, null, 12, 24, null, 7 },
                    { 20, null, 8, 96, null, 2 },
                    { 21, null, 9, 96, null, 2 },
                    { 22, null, 10, 96, null, 2 },
                    { 23, null, 11, 96, null, 2 },
                    { 24, null, 12, 96, null, 2 },
                    { 25, 1, null, 96, null, 1 },
                    { 26, 1, null, 48, null, 3 },
                    { 27, 1, null, 24, null, 5 },
                    { 28, 3, null, 24, null, 5 },
                    { 29, 4, null, 48, null, 3 },
                    { 30, 4, null, 24, null, 5 },
                    { 31, 3, null, 96, null, 1 },
                    { 32, 4, null, 96, null, 1 },
                    { 33, null, 1, 19, null, 4 },
                    { 34, null, 1, 19, null, 4 },
                    { 35, null, 2, 19, null, 4 },
                    { 36, null, 2, 19, null, 4 },
                    { 37, null, 3, 19, null, 4 },
                    { 38, null, 3, 19, null, 4 },
                    { 39, null, 1, 12, null, 6 },
                    { 40, null, 1, 12, null, 6 },
                    { 41, null, 4, 19, null, 4 },
                    { 42, null, 4, 19, null, 4 },
                    { 43, null, 4, 12, null, 6 },
                    { 44, null, 4, 12, null, 6 },
                    { 45, null, 5, 19, null, 4 },
                    { 46, null, 5, 19, null, 4 },
                    { 47, null, 5, 12, null, 6 },
                    { 48, null, 5, 12, null, 6 },
                    { 49, null, 6, 19, null, 4 },
                    { 50, null, 6, 19, null, 4 },
                    { 51, null, 6, 12, null, 6 },
                    { 52, null, 6, 12, null, 6 },
                    { 53, null, 7, 19, null, 4 },
                    { 54, null, 7, 19, null, 4 },
                    { 55, null, 7, 12, null, 6 },
                    { 56, null, 7, 12, null, 6 },
                    { 57, null, 8, 19, null, 4 },
                    { 58, null, 8, 19, null, 4 },
                    { 59, null, 8, 12, null, 6 },
                    { 60, null, 8, 12, null, 6 },
                    { 61, null, 9, 19, null, 4 },
                    { 62, null, 9, 19, null, 4 },
                    { 63, null, 9, 12, null, 6 },
                    { 64, null, 9, 12, null, 6 },
                    { 65, null, 10, 19, null, 4 },
                    { 66, null, 10, 19, null, 4 },
                    { 67, null, 10, 19, null, 4 },
                    { 68, null, 10, 19, null, 4 },
                    { 69, null, 11, 19, null, 4 },
                    { 70, null, 11, 19, null, 4 },
                    { 71, null, 11, 12, null, 6 },
                    { 72, null, 11, 12, null, 6 },
                    { 73, null, 12, 19, null, 4 },
                    { 74, null, 12, 19, null, 4 },
                    { 75, null, 12, 12, null, 6 },
                    { 76, null, 12, 12, null, 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_DistributionForSubjects_FlowId",
                table: "DistributionForSubjects",
                column: "FlowId");

            migrationBuilder.CreateIndex(
                name: "IX_DistributionForSubjects_GroupId",
                table: "DistributionForSubjects",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_DistributionForSubjects_ProfessorId",
                table: "DistributionForSubjects",
                column: "ProfessorId");

            migrationBuilder.CreateIndex(
                name: "IX_DistributionForSubjects_SubjectId",
                table: "DistributionForSubjects",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_FlowModelGroupModel_GroupsId",
                table: "FlowModelGroupModel",
                column: "GroupsId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DistributionForSubjects");

            migrationBuilder.DropTable(
                name: "FlowModelGroupModel");

            migrationBuilder.DropTable(
                name: "Professors");

            migrationBuilder.DropTable(
                name: "Subjects");

            migrationBuilder.DropTable(
                name: "Flows");

            migrationBuilder.DropTable(
                name: "Groups");
        }
    }
}
