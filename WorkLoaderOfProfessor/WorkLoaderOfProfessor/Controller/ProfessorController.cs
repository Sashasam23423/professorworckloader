﻿using Microsoft.AspNetCore.Mvc;
using WorkLoaderOfProfessor.Service.GeneAlgoritm;
using WorkLoaderOfProfessor.Service.ProfessorService;

namespace WorkLoaderOfProfessor.Controller
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProfessorController : ControllerBase
    {
        private readonly IProfessorService _professorService;
        private readonly IGeneAlgoritm _geneAlgoritm;
        public ProfessorController(IProfessorService professorService, IGeneAlgoritm geneAlgoritm)
        {
            _professorService = professorService;
            _geneAlgoritm = geneAlgoritm;
        }

        [HttpPost]
        public async Task<IActionResult> StartAlgoritm() 
        {
            await _geneAlgoritm.Run();
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetProfessors()
        {
            return Ok(await _professorService.GetProfessors());
        }

        [HttpGet]
        public async Task<IActionResult> GetProfessor(int id)
        {
            return Ok(await _professorService.GetProfessor(id));
        }
       
    }
}
