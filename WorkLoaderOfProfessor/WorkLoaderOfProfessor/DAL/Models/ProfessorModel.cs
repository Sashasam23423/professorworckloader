﻿using System.ComponentModel.DataAnnotations;

namespace WorkLoaderOfProfessor.DAL.Models
{
    public class ProfessorModel
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string? MiddleName { get; set; }
        public string LastName { get; set; } = string.Empty;
        public string Post { get; set; } = string.Empty;
        public float Like { get; set; } = 0;
        public float Hours { get; set; } = 0.0f;
        public int NumbersOfFlows { get; set; } = 0;
        public int NumbersOfGroups { get; set; } = 0;
        public int NumberOfHalfGroup { get; set; } = 0;
        public int CountOfStudent { get; set; } = 0;
    }
}
