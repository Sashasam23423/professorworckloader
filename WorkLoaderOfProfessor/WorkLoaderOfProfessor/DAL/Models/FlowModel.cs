﻿

namespace WorkLoaderOfProfessor.DAL.Models
{
    public class FlowModel
    {
        public int Id { get; set; }
        public int NumberStream { get; set; }
        public int Half { get; set; }
        public List<GroupModel> Groups { get; set; }
    }
}
