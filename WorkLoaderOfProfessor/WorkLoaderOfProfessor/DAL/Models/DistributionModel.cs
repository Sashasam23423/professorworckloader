﻿namespace WorkLoaderOfProfessor.DAL.Models
{
    public class DistributionModel
    {
        public int Id { get; set; }
        public int SubjectId { get; set; }
        public SubjectModel Subject { get; set; }
        public int? GroupId { get; set; }
        public GroupModel Group { get; set; }
        public int? FlowId { get; set; }
        public FlowModel Flow { get; set; }
        public int? ProfessorId { get; set; }
        public ProfessorModel Professor { get; set; }
        public int CountStudent { get; set; }
        public int Semester { get; set; }
        public float Hours { get; set; }
    }
}
