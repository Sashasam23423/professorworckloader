﻿namespace WorkLoaderOfProfessor.DAL.Models
{
    public class ProfessorDoesntLikeSubject
    {
        public int Id { get; set; }
        public float Happy { get; set; }
        public int ProfessorId { get; set; }
        public ProfessorModel Professor { get; set; }
        public int SubjectId { get; set; }
        public SubjectModel Subject { get; set; }
    }
}
