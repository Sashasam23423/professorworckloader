﻿using System.ComponentModel.DataAnnotations;
namespace WorkLoaderOfProfessor.DAL.Models
{
    public class GroupModel
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string DirectionOfStudy { get; set; } = string.Empty;
        public int Half { get; set; }
        public int CountStudent { get; set; }
        public List<FlowModel> Flows { get; set; }
    }
}
