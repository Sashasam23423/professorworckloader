﻿using System.ComponentModel.DataAnnotations;

namespace WorkLoaderOfProfessor.DAL.Models
{
    public class SubjectModel
    {
        [Key] 
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public string Type { get; set; } = string.Empty;
    }
}
