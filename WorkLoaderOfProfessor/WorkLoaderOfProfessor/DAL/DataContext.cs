﻿using Microsoft.EntityFrameworkCore;
using WorkLoaderOfProfessor.DAL.Models;

namespace WorkLoaderOfProfessor.DAL
{
    public class DataContext : DbContext
    {
        public DbSet<GroupModel> Groups { get; set; }
        public DbSet<DistributionModel> DistributionForSubjects{ get; set; }
        public DbSet<FlowModel> Flows { get; set; }
        public DbSet<ProfessorModel> Professors { get; set; }
        public DbSet<SubjectModel> Subjects { get; set; }
        public DbSet<ProfessorDoesntLikeSubject> ProfessorDoesntLikeSubjects { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql("Host=localhost; Port=5432; Database=ProfessorWorkLoader; Username=root; Password=root");
        }
    }
}
