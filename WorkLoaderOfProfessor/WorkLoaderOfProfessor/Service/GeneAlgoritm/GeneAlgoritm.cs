﻿using Microsoft.EntityFrameworkCore;
using WorkLoaderOfProfessor.DAL;
using WorkLoaderOfProfessor.DAL.Models;
using WorkLoaderOfProfessor.Service.FirstHandDistribution;

namespace WorkLoaderOfProfessor.Service.GeneAlgoritm
{
    public class GeneAlgoritm : IGeneAlgoritm
    {
        private readonly IFirstHandDistribution _firstHandDistribution;
        private readonly DataContext _context;
        public List<Individual> population;
        private List<Individual> _offspring;
        private List<ProfessorModel> _professors;
        private List<ProfessorDoesntLikeSubject> _professorsLike;
        public GeneAlgoritm(IFirstHandDistribution firstHandDistribution, DataContext context)
        {
            _firstHandDistribution = firstHandDistribution;
            population = new List<Individual>();
            _offspring = new List<Individual>();
            _context = context;
        }

        public async Task<Individual> Run()
        {
            List<Individual> bestIndividuals = new List<Individual>();
            _professors = await _context.Professors.ToListAsync();
            _professorsLike = await _context.ProfessorDoesntLikeSubjects.ToListAsync();
            var subjects = await _context.Subjects.ToListAsync();
            var groups = await _context.Groups.ToListAsync();
            Consts consts = new Consts();

            for (int i = 0; i < consts.MAX_SIZE_POPULATION; i++)
            {
                var genes = await _firstHandDistribution.DistributionAsync();
                population.Add(new Individual(genes));
            }
            // подсчитывание приспособленности
            foreach (var individual in population)
            {
                individual.CalculateFitnessMin(_professors, subjects);
            }
            int index = -1;
            Individual bestIndividual = new();
            bestIndividual = population[0].Clone();

            foreach (var individual in population)
            {
                if (individual.fitnessMin < bestIndividual.fitnessMin && individual.countHeavyFine == 0)
                {
                    bestIndividuals.Add(individual.Clone());
                }
            }
            while (index < consts.MAX_SIZE_AGE)
            {
                
                index++;
                // отбор
                SelTournament();
                Random random = new Random();
                // скрещивание
                for (var i = 0; i < _offspring.Count; i++)
                {

                    if (random.NextDouble() <= 0.9)
                    {
                        cxIndividaul(_offspring[i], _offspring[i]);
                    }

                }
                // мутация
                for (var i = 0; i < _offspring.Count; i++)
                {
                    if (random.NextDouble() <= 0.1)
                    {
                        Mutation(_offspring[i], 0.1);
                    }

                }
                // подсчитывание приспособленности
                foreach (var individual in _offspring)
                {
                    individual.CalculateFitnessMin(_professors, subjects);
                    Console.WriteLine($"{individual.fitnessMin} : {individual.countHeavyFine}");
                }
                for (var i = 0; i < _offspring.Count; i++)
                {
                    if (_offspring[i].indexChange != 0)
                    {
                        population[_offspring[i].indexChange] = _offspring[i].Clone();
                        population[_offspring[i].indexChange].indexChange = 0;
                    }
                }
                _offspring = new List<Individual>();
                foreach (var individual in population)
                {
                    if (individual.fitnessMin < bestIndividual.fitnessMin && individual.countHeavyFine == 0)
                    {
                        bestIndividuals.Add(individual.Clone());
                    }
                }
            }
 
            
            var distributions = await _context.DistributionForSubjects.ToListAsync();

            foreach (var gene in bestIndividual.values)
            {
                var distribution = distributions.FirstOrDefault(d => d.Id == gene.distributionId);
                distribution.ProfessorId = gene.professorId;
                _context.DistributionForSubjects.Update(distribution);
            }

            await _context.SaveChangesAsync();

            foreach (var professor in _professors)
            {
                professor.Hours = 0;
                professor.NumbersOfGroups = 0;
                professor.NumbersOfFlows = 0;
                professor.NumberOfHalfGroup = 0;
                professor.CountOfStudent = 0;
                professor.Like = 0;
            }

            
            
            foreach (var professor in _professors)
            {
                var bestGenes = bestIndividual.values.Where(b => b.professorId == professor.Id);
                foreach (var gene in bestGenes)
                {
                    var subject = subjects.FirstOrDefault(s => s.Id == gene.subjectId);
                    var distribution = distributions.FirstOrDefault(d => d.Id == gene.distributionId);
                    var group = groups.FirstOrDefault(g => g.Id == distribution.GroupId);

                    professor.Hours += gene.hours;
                    professor.Like += gene.happy;
                    professor.CountOfStudent += distribution.CountStudent;

                    switch (subject.Type)
                    {
                        case "Лекционное занятие":
                            professor.NumbersOfFlows++;
                            break;

                        case "Приктическое занятие":
                            professor.NumbersOfGroups++;
                            break;

                        case "Лабораторное занятие":
                            professor.NumberOfHalfGroup++;
                            break;
                    }    
                }
                professor.Like /= bestGenes.Count();
                var like = professor.Like * 10;
                int likeRound = (int)like + 1;
                professor.Like = ((float)likeRound) / 10;
                _context.Professors.Update(professor);
            }
            await _context.SaveChangesAsync();
            return bestIndividual;
        }


        public void SelTournament()
        {
            Random random = new Random();
            int individualIndex_1 = 0;
            int individualIndex_2 = 0;
            int individualIndex_3 = 0;
            for (int i  = 0; i < population.Count; i++) 
            {
                while (individualIndex_1.Equals(individualIndex_2) || individualIndex_1.Equals(individualIndex_3) || individualIndex_2.Equals(individualIndex_3) )
                {
                    individualIndex_1 = random.Next(0, population.Count - 1);
                    individualIndex_2 = random.Next(0, population.Count - 1);
                    individualIndex_3 = random.Next(0, population.Count - 1);
                }
                var individual = FindMinFitnessForSel(population[individualIndex_1], FindMinFitnessForSel(population[individualIndex_2], population[individualIndex_3]));
                _offspring.Add(individual);
                int index = population.FindIndex(i => i.Equals(individual));
                _offspring[_offspring.Count-1].indexChange = index;
                individualIndex_1 = 0;
                individualIndex_2 = 0;
                individualIndex_3 = 0;
            }
        }

        public Individual FindMinFitnessForSel( Individual individual_1, Individual individual_2)
        {
            return individual_1.fitnessMin < individual_2.fitnessMin ? individual_1 : individual_2;
        }

        public void cxIndividaul(Individual childFirst, Individual childSecond)
        {
            Random random = new Random();
            int indexFirst = random.Next(0, childFirst.values.Count);
            int indexSecond = random.Next(0, childSecond.values.Count);
            
            while (indexFirst.Equals(indexSecond))
            {
                indexFirst = random.Next(0, childFirst.values.Count);
                indexSecond = random.Next(0, childSecond.values.Count);
            }
            
            (childFirst.values[indexFirst].professorId, 
             childSecond.values[indexSecond].professorId) = (childSecond.values[indexSecond].professorId, 
                                                           childFirst.values[indexFirst].professorId);
            
        }
  
        public void Mutation(Individual mutant, double indpb)
        {
            
            Random random = new Random();
            int indexFirst = 0;
            int indexSecond = 0;
            for (var i = 0; i < mutant.values.Count; i++)
            {
                while (indexFirst.Equals(indexSecond))
                {
                    indexFirst = random.Next(0, mutant.values.Count);
                    indexSecond = random.Next(0, mutant.values.Count);
                }

                if (random.NextDouble() < indpb)
                {
                    (mutant.values[indexFirst].professorId, 
                     mutant.values[indexSecond].professorId) = (mutant.values[indexSecond].professorId, 
                                                                    mutant.values[indexFirst].professorId);
                }
            }
        } 
    }
}
