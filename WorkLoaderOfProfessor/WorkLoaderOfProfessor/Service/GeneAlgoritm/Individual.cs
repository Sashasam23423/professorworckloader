﻿using WorkLoaderOfProfessor.DAL.Models;
using WorkLoaderOfProfessor.Service.GeneAlgoritm.Models.Gene;

namespace WorkLoaderOfProfessor.Service.GeneAlgoritm
{
    public class Individual : IIndividual
    {
        public List<Gene> values { get; set; } = new List<Gene>();
        public float fitnessMin { get; set; }
        public int countHeavyFine { get; set; }
        public int indexChange = 0;
        public Individual(List<Gene> genes)
        {
            foreach (var item in genes)
            {
                values.Add(item.Clone());
            }
            fitnessMin = 0.0f;
        }

        public Individual()
        {
        }

        public Individual(float fitnessMin, int countHeavyFine, List<Gene> values)
        {
            this.fitnessMin = fitnessMin;
            this.countHeavyFine = countHeavyFine;
            foreach (var item in values)
            {
                this.values.Add(item.Clone());
            }
        }

        public Individual Clone()
        {
            return new Individual(fitnessMin, countHeavyFine, values);
        }

        public void CalculateFitnessMin(List<ProfessorModel> professors, List<SubjectModel> subjects)
        {
            countHeavyFine = 0;
            fitnessMin = 0;
            Consts consts = new(); 
            foreach (var professor in professors)
            {
                var hoursSum = CountHours(values.Where(p => p.professorId.Equals(professor.Id)), subjects, professor);
                if(hoursSum > consts.MAX_HOURS || hoursSum < consts.MIN_HOURS)
                {
                    fitnessMin += consts.HEAVY_FINE;
                    countHeavyFine++;
                }
            }
            
        }

        private float CountHours(IEnumerable<Gene> geneGroupedByProfessor, List<SubjectModel> subjects, ProfessorModel professor)
        {
            float hoursSum = 0;
            Consts consts = new();
            foreach (var gene in geneGroupedByProfessor)
            {
                var subject = subjects.FirstOrDefault(s => s.Id == gene.subjectId);
                if (subject.Type == "Вне аудиторная")
                    if(professor.Post != "Профессор" || professor.Post != "Доцент")
                    {
                        fitnessMin += consts.HEAVY_FINE;
                        countHeavyFine++;
                    }
                hoursSum += gene.hours;
                fitnessMin += gene.happy;
            }
            return hoursSum;
        }
    }
}
