﻿namespace WorkLoaderOfProfessor.Service.GeneAlgoritm.Models.Gene
{
    public class Gene
    {
        public int professorId { get; set; }
        public int subjectId { get; set; }  
        public int distributionId { get; set; }
        public float hours { get; set; }
        public float happy { get; set; }
        public Gene(int professorId, int distributionId, int subjectId, float hours, float happy)
        {
            this.professorId = professorId;
            this.distributionId = distributionId;
            this.subjectId = subjectId;
            this.happy = happy;
            this.hours = hours;
        }
        public Gene(Gene obj)
        {
            this.professorId = obj.professorId;
            this.distributionId = obj.distributionId;
            this.subjectId = obj.subjectId;
            this.happy = obj.happy;
            this.hours = obj.hours;
        }
        public Gene Clone()
        {
            return new Gene(this.professorId, this.distributionId, this.subjectId, this.hours,  this.happy);
        }
    }
}
