﻿namespace WorkLoaderOfProfessor.Service.GeneAlgoritm
{
    public interface IGeneAlgoritm
    {
        Task<Individual> Run();
    }
}
