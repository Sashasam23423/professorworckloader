﻿using WorkLoaderOfProfessor.DAL.Models;

namespace WorkLoaderOfProfessor.Service.GeneAlgoritm
{
    public interface IIndividual
    {
        void CalculateFitnessMin(List<ProfessorModel> professors, List<SubjectModel> subjects);
    }
}
