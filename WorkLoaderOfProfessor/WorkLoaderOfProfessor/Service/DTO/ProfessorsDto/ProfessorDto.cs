﻿

namespace WorkLoaderOfProfessor.Service.DTO.ProfessorsDto
{
    public class ProfessorDto
    {
        // for all
        public int Id { get; set; }
        public string subjectName { get; set; }
        public string subjectType { get; set; }
        public int Semester { get; set; }
        public int CountStudent { get; set; }
        public float Hours { get; set; }

        // for lecturer
        public int FlowsCount { get; set; }
        // for Practic
        public int GroupsCount { get; set; }
        // for lab
        public int HalfGroupsCount { get; set; }
       
    }
}
