﻿namespace WorkLoaderOfProfessor.Service.DTO.ProfessorsDto
{
    public class ProfessorsDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string? MiddleName { get; set; }
        public string LastName { get; set; }
        public string Post { get; set; }
        public float Like { get; set; }
        public float Hours { get; set; }
        public int NumbersOfFlows { get; set; }
        public int NumbersOfGroups { get; set; }
        public int NumberOfHalfGroup { get; set; }
        public int CountOfStudent { get; set; }
    }
}
