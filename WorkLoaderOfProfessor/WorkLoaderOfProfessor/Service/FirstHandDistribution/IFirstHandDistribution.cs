﻿using WorkLoaderOfProfessor.Service.GeneAlgoritm.Models.Gene;

namespace WorkLoaderOfProfessor.Service.FirstHandDistribution
{
    public interface IFirstHandDistribution
    {
        Task<List<Gene>> DistributionAsync();
    }
}
