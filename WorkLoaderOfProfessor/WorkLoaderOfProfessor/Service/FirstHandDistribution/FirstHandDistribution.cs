﻿using Microsoft.EntityFrameworkCore;
using WorkLoaderOfProfessor.DAL;
using WorkLoaderOfProfessor.DAL.Models;
using WorkLoaderOfProfessor.Service.GeneAlgoritm.Models.Gene;

namespace WorkLoaderOfProfessor.Service.FirstHandDistribution
{
    public class FirstHandDistribution : IFirstHandDistribution
    {
        private readonly DataContext _context;
        public FirstHandDistribution(DataContext context)
        {
            _context = context;
        }

        public async Task<List<Gene>> DistributionAsync()
        {
            bool exit = false;
            Random rnd = new();
            List<Gene> genes = new();
            List<ProfessorDoesntLikeSubject> professorDoesntLikeSubjects = await _context.ProfessorDoesntLikeSubjects.ToListAsync();
            var subjects = await _context.Subjects.ToListAsync();
            while (!exit)
            {
                var professorsList = await _context.Professors.ToListAsync();
                List<DistributionModel> distributions = await _context.DistributionForSubjects.ToListAsync();

                foreach(var professor in professorsList) 
                {
                    professor.Hours = 0;
                    professor.NumbersOfGroups = 0;
                    professor.NumbersOfFlows = 0;
                    professor.NumberOfHalfGroup = 0;
                    professor.CountOfStudent = 0;
                }

                Consts consts = new();
                int ITERATIONS = 100;
                int iterations = 0;
                foreach (var professor in professorsList)
                {
                    iterations = 0;

                    while (professor.Hours <= consts.MIN_HOURS)
                    {
                        if (iterations == ITERATIONS)
                            break;

                        int index = rnd.Next(0, distributions.Count);

                        if (distributions.Count == 0 || 
                            professor.Hours + distributions[index].Hours > consts.MAX_HOURS)
                            break;

                        var subject = subjects.FirstOrDefault(s => s.Id == distributions[index].SubjectId);

                        if (subject.Type == "Внеаудиторная" && (professor.Post != "Профессор" || professor.Post != "Доцент")) 
                        { 
                            continue; 
                        }

                        iterations++;
                        professor.Hours += distributions[index].Hours;
                        Gene gene = new(professor.Id,
                            distributions[index].Id, 
                            distributions[index].SubjectId, 
                            distributions[index].Hours, 
                            professorDoesntLikeSubjects.FirstOrDefault(p => p.ProfessorId == professor.Id
                                                                       && p.SubjectId == distributions[index].SubjectId).Happy);
                        genes.Add(gene);
                        distributions.Remove(distributions[index]);

                    }
                }

                iterations = 0;

                while (distributions.Count != 0)
                {
                    if (iterations == ITERATIONS)
                        break;
                    foreach (var professor in professorsList)
                    {
                        int index = rnd.Next(0, distributions.Count);

                        if (distributions.Count == 0)
                            break;

                        if (professor.Hours + distributions[index].Hours > consts.MAX_HOURS)
                            continue;

                        var subject = subjects.FirstOrDefault(s => s.Id == distributions[index].SubjectId);

                        if (subject.Type == "Внеаудиторная" && (professor.Post != "Профессор" || professor.Post != "Доцент"))
                            continue;
                        
                        professor.Hours += distributions[index].Hours;
                        Gene gene = new(professor.Id, 
                            distributions[index].Id, 
                            distributions[index].SubjectId, 
                            distributions[index].Hours, 
                            professorDoesntLikeSubjects.FirstOrDefault(p => p.ProfessorId == professor.Id
                                                                       && p.SubjectId == distributions[index].SubjectId).Happy);
                        genes.Add(gene);
                        distributions.Remove(distributions[index]);
                    }
                    iterations++;
                }

                if (distributions.Count == 0)
                    exit = true;
                else
                    genes = new(); 
            }
            return genes;
        }
    }
}
