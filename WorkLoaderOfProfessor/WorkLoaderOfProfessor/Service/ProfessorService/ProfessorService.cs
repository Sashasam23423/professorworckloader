﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using WorkLoaderOfProfessor.DAL;
using WorkLoaderOfProfessor.Service.DTO.ProfessorsDto;

namespace WorkLoaderOfProfessor.Service.ProfessorService
{
    public class ProfessorService : IProfessorService
    {
        private readonly DataContext _context;
        private readonly IMapper _mapper;
        public ProfessorService(DataContext dataContext, IMapper mapper)
        {
            _context = dataContext;
            _mapper = mapper;
        }

        public async Task<List<ProfessorDto>> GetProfessor(int professorId)
        {
            
            var distributions = _context.DistributionForSubjects.Where(d => d.ProfessorId == professorId);
            var subjects = await _context.Subjects.ToListAsync();
            List<ProfessorDto> professorDtos = new List<ProfessorDto>();

            foreach (var subject in subjects)
            {
                for (int i = 1; i < 9; i++)
                {
                    var distributionsBySemestrAndSubjects = distributions.Where(d => d.Semester == i && d.SubjectId == subject.Id);
                    ProfessorDto professorDto = new();
                    foreach (var distribution in distributionsBySemestrAndSubjects)
                    { 
                        professorDto.Hours += distribution.Hours;
                        professorDto.CountStudent += distribution.CountStudent;
                        professorDto.subjectName = distribution.Subject.Name;
                        professorDto.subjectType = distribution.Subject.Type;
                        professorDto.Id = distribution.Id;
                        professorDto.Semester = distribution.Semester;
                        switch(subject.Type)
                        {
                            case "Лекционное занятие":
                                professorDto.FlowsCount++;
                                break;

                            case "Приктическое занятие":
                                professorDto.GroupsCount++;
                                break;

                            case "Лабораторное занятие":
                                professorDto.HalfGroupsCount++;
                                break;
                        }
                    }
                    if(professorDto.Id != 0)
                        professorDtos.Add(professorDto);
                }
            }
            return professorDtos;
        }

        public async Task<List<ProfessorsDto>> GetProfessors()
        {
            return _mapper.Map<List<ProfessorsDto>>(await _context.Professors.ToListAsync());
        }
    }
}
