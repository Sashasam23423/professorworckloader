﻿using WorkLoaderOfProfessor.Service.DTO.ProfessorsDto;

namespace WorkLoaderOfProfessor.Service.ProfessorService
{
    public interface IProfessorService
    {
        Task<List<ProfessorsDto>> GetProfessors();
        Task<List<ProfessorDto>> GetProfessor(int professorId);
    }
}
