﻿namespace WorkLoaderOfProfessor.Service
{
    public class Consts
    {
        public float MAX_HOURS { get;}
        public float MIN_HOURS { get; }
        public float HEAVY_FINE { get; }
        public float EAZY_FINE { get; }
        public float MAX_SIZE_POPULATION { get; }
        public float MAX_SIZE_AGE { get; }
        public Consts() 
        {
            MAX_HOURS = 900;
            MIN_HOURS = 840;
            HEAVY_FINE = 100;
            EAZY_FINE = 10;
            MAX_SIZE_AGE = 40;
            MAX_SIZE_POPULATION = 100;
        }
    }
}
