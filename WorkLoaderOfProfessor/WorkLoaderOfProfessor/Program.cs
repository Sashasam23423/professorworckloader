
using WorkLoaderOfProfessor.DAL;
using WorkLoaderOfProfessor.Service.FirstHandDistribution;
using WorkLoaderOfProfessor.Service.GeneAlgoritm;
using WorkLoaderOfProfessor.Service.ProfessorService;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddHttpContextAccessor();

builder.Services.AddDbContext<DataContext>();
builder.Services.AddScoped<IFirstHandDistribution, FirstHandDistribution>();
builder.Services.AddScoped<IGeneAlgoritm, GeneAlgoritm>();
builder.Services.AddScoped<IProfessorService, ProfessorService>();
builder.Services.AddAutoMapper(typeof(Program).Assembly);
builder.Services.AddCors(setupAction => setupAction
    .AddDefaultPolicy(setupAction => setupAction
        .WithOrigins("http://localhost:3000")
        .AllowCredentials()
        .AllowAnyMethod()
        .AllowAnyHeader()));



var app = builder.Build();
app.MapControllers();
app.UseRouting();
app.UseCors();
app.UseHttpsRedirection();
app.UseAuthorization();
app.Run();