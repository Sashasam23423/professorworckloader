import ProfessorsPageComponent from "./components/professorsPageComponent/ProfessorsPageComponent";
import ProfessorPageComponent from "./components/professorPageComponent/ProfessorPageComponent";
import {Route, Routes} from "react-router-dom";
import {observer} from "mobx-react-lite";


function App() {
  return (
    <div className="App">
        <Routes>
            <Route path="/" element={<ProfessorsPageComponent/>}/>
            <Route path="/professor" element={<ProfessorPageComponent />}/>
        </Routes>


    </div>
  );
}

export default observer(App);
