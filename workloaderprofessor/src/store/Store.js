import {makeAutoObservable} from "mobx"
import {GetProfessor, GetProfessorsList, StartGenerate} from "../service/professorService";

class  Store
{
    professors = []
    professor = []

    constructor() {
        makeAutoObservable(this)
    }

    async GetProfessors()
    {
        try {
            const responce =  GetProfessorsList();
            responce.then( (result) => {
                this.professors = (result.data)
            })
        }
        catch (e)
        {

        }
    }


    async GetProfessor()
    {
        try {
            const  responce = GetProfessor(localStorage.getItem('professorId'))
            responce.then( (result) => {
                this.professor = (result.data)
            })
        }
        catch (e)
        {

        }
    }


    async StartGenerate()
    {
        try {
            StartGenerate()
            const responce = GetProfessorsList();
            responce.then( (result) => {
                this.professors = (result.data)
            })
        }
        catch (e)
        {

        }
    }

}
export default new Store()