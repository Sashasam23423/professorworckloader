import React from "react";
import {Link} from "react-router-dom";

export const API_URL = "https://localhost:7190/api/"

export const columnsProfessorsTable = [
    {
        title: 'Фамилия',
        dataIndex: 'lastName',
        key: 'lastName',
        sorter: (a, b) => a.lastName.localeCompare(b.lastName) ,
        sortDirections: ['ascend'],
        render: (data) => <Link  to="/professor" style={{color: "black",}}>{data}</Link>,
        onCell: (professor) =>{
            return {
                onClick:(event) => {
                    localStorage.setItem("professorId", professor.id)
                }
            }
        }
    },

    {
        title: 'Имя',
        dataIndex: 'name',
        key: 'name',
        sorter: (a, b) => a.name.localeCompare(b.name) ,
        sortDirections: ['ascend'],
    },

    {
        title: 'Отчество',
        dataIndex: 'middleName',
        key: 'middleName',
        sorter: (a, b) => a.middleName.localeCompare(b.middleName) ,
        sortDirections: ['ascend'],
    },

    {
        title: 'Должность',
        dataIndex: 'post',
        key: 'post',
        sorter: (a, b) => a.post.localeCompare(b.post) ,
    },

    {
        title: 'Часы',
        dataIndex: 'hours',
        key: 'hours',
        sorter: (a, b) => a.hours - b.hours,
    },

    {
        title: 'Количество потоков',
        dataIndex: 'numbersOfFlows',
        key: 'numbersOfFlows',
        sorter: (a, b) => a.numbersOfFlows - b.numbersOfFlows,
    },

    {
        title: 'Количество групп',
        dataIndex: 'numbersOfGroups',
        key: 'numbersOfGroups',
        sorter: (a, b) => a.numbersOfGroups - b.numbersOfGroups,
    },

    {
        title: 'Количество подгурпп',
        dataIndex: 'numberOfHalfGroup',
        key: 'numberOfHalfGroup',
        sorter: (a, b) => a.numberOfHalfGroup - b.numberOfHalfGroup,
    },

    {
        title: 'Количество студентов',
        dataIndex: 'countOfStudent',
        key: 'countOfStudent',
        sorter: (a, b) => a.countOfStudent - b.countOfStudent,
    },

    {
        title: 'Настроение',
        dataIndex: 'like',
        key: 'like',
        sorter: (a, b) => a.like - b.like,
    },
];


export const columnsProfessorTable = [
    {
        title: 'Дисциплина',
        dataIndex: 'subjectName',
        key: 'subjectName',
        sorter: (a, b) => a.subjectName.localeCompare(b.subjectName) ,
        sortDirections: ['ascend'],
    },

    {
        title: 'Вид дисциплины',
        dataIndex: 'subjectType',
        key: 'subjectType',
        sorter: (a, b) => a.subjectType.localeCompare(b.subjectType) ,
        sortDirections: ['ascend'],
    },

    {
        title: 'Семестр',
        dataIndex: 'semester',
        key: 'semester',
        sorter: (a, b) => a.semester - b.semester,
    },

    {
        title: 'Количество потоков',
        dataIndex: 'flowsCount',
        key: 'flowsCount',
        sorter: (a, b) => a.flowsCount - b.flowsCount,
    },

    {
        title: 'Количество групп',
        dataIndex: 'groupsCount',
        key: 'groupsCount',
        sorter: (a, b) => a.groupsCount - b.groupsCount,
    },

    {
        title: 'Количество подгрупп',
        dataIndex: 'halfGroupsCount',
        key: 'halfGroupsCount',
        sorter: (a, b) => a.halfGroupsCount - b.halfGroupsCount,
    },

    {
        title: 'Количество студентов',
        dataIndex: 'countStudent',
        key: 'countStudent',
        sorter: (a, b) => a.countStudent - b.countStudent,
    },

    {
        title: 'Часы',
        dataIndex: 'hours',
        key: 'hours',
        sorter: (a, b) => a.hours - b.hours,
    },
];


