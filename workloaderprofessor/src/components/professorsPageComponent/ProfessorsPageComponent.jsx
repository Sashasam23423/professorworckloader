import React, {useEffect, useState} from 'react';
import {Button, Table} from 'antd';
import {observer} from "mobx-react-lite";
import {columnsProfessorsTable} from '../../Consts'
import store from "../../store/Store";

const ProfessorsPageComponent = () => {
    const [maxAge, setMaxAge] = useState(40)
    const [maxPopulationSize, setMaxPopulationSize] = useState(100)

    useEffect( () => {
            store.GetProfessors()
        }, [store.professors]);

    return (
        <div>
            <Button style={{marginLeft: "10px", marginBottom: "10px"}} onClick={()=>{
                store.StartGenerate()
            }} type="primary">Сгенерировать</Button>
            <Table  dataSource={store.professors} columns={columnsProfessorsTable} rowKey = "id" pagination={100}/>
        </div>
    );
};

export default observer(ProfessorsPageComponent);