import {useEffect} from 'react';
import {Table} from 'antd';
import {observer} from "mobx-react-lite";
import {columnsProfessorTable} from "../../Consts";
import store from "../../store/Store";


const ProfessorPageComponent = () => {
    useEffect(()=>{
        store.GetProfessor()
    },[])

    return (
        <div>

            <Table  dataSource={store.professor} columns={columnsProfessorTable} rowKey = "id" pagination={100}/>
        </div>
    );
};

export default observer(ProfessorPageComponent);