import $api from "../http";

export async function GetProfessorsList()
{
    return $api.get("/Professor/GetProfessors")
}

export async function GetProfessor(id)
{
    return $api.get(`Professor/GetProfessor?id=${id}`)
}

export async function StartGenerate()
{
    return $api.post(`Professor/StartAlgoritm`)
}